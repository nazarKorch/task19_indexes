 DELIMITER //
     CREATE TRIGGER BeforeInsertEmployee
     BEFORE INSERT
     ON employee FOR EACH ROW
     BEGIN
       IF new.post <> ANY(SELECT post FROM post) OR (SELECT post FROM post) IS NULL
       THEN SIGNAL SQLSTATE '45000'
       SET MESSAGE_TEXT = 'post with this name doesnt exist';
	END IF;
    END
    //

DELIMITER //
CREATE TRIGGER BeforeDeletePost
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
	SIGNAL SQLSTATE '45000'
	SET MESSAGE_TEXT = 'post is primary';
END
//

DELIMITER //
CREATE TRIGGER BeforeInsertIdentity
 BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
IF new.identity_number NOT RLIKE '^[0-9]{10}$'
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'wrong identity';
END IF;
END
//

DELIMITER //
CREATE TRIGGER BeforeDeleteStreet
BEFORE DELETE
ON street FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street) < 2
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'MIN cardinality';
END IF;
END //

DELIMITER //
CREATE TRIGGER BeforeInsertStreet
BEFORE INSERT
ON street FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street) > 5
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'MAX cardinality';
END IF;
END //
Query OK, 0 rows affected (0.13 sec)

DELIMITER //
 CREATE TRIGGER AfterDeleteStreet
AFTER DELETE
ON street FOR EACH ROW
BEGIN
IF(SELECT COUNT(*) FROM street) < 2
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'MIN cardinality';
END IF;
END //

DELIMITER //
CREATE FUNCTION showInitials(id1 INT)
RETURNS varchar(5)
BEGIN
DECLARE s VARCHAR(5);
DECLARE x VARCHAR(1);
SET s = (SELECT LEFT(name,1) FROM employee WHERE id = id1);
SET x = (SELECT LEFT(surname,1) FROM employee WHERE id = id1);
SET s = CONCAT(s,'.',x,'.');
RETURN s;
END //

SELECT showInitials(e.id) initials, e.post FROM employee e;




